﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Low.Controllers
{
    public class QuestionnaireController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Scales = false;
            ViewBag.Horns = false;
            ViewBag.City = false;
            ViewBag.Works = false;
            return View();
        }
        [HttpPost]
        public ActionResult UserPassedData(FormCollection form)
        {
            ViewBag.TextReturn = "Your input fullname: "+ form["Text"];
            ViewBag.AdressReturn = "Your input adress: " + form["Adress"];
            ViewBag.ScalesReturn = "You do not have scales";
            ViewBag.HornsReturn = "You do not have horns";
            ViewBag.RegionReturn = "Your input region: " + form["Region"];
            ViewBag.OrganisationReturn = "Your input organisation: " + form["Organisation"];
            ViewBag.EmailReturn = "Your input email: " + form["Email"];
            if (Convert.ToBoolean(form["Check-box-scales"].Split(',')[0])) 
            {
                ViewBag.ScalesReturn = "You have scales";
            }
            if (Convert.ToBoolean(form["Check-box-horns"].Split(',')[0]))
            {
                ViewBag.HornsReturn = "You have horns";
            }
            ViewBag.CityReturn = "You did not checked city";
                if (form["Check-box-city"]!=null && Convert.ToBoolean(form["Check-box-city"].Split(',')[0]))
                {
                    ViewBag.CityReturn = "You checked city";
                }
            ViewBag.WorkReturn = "You did not checked work";
            if (form["Check-box-work"] != null && Convert.ToBoolean(form["Check-box-work"].Split(',')[0]))
            {
                ViewBag.WorkReturn = "You checked work";
            }
            return View();
        }
    }
}